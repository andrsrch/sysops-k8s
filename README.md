# Sysops K8s

## AWS Credentials
Set access infomation to AWS account in environment variables.

**Windows CMD**
```
set AWS_ACCESS_KEY_ID="<user_access_key>"
set AWS_SECRET_ACCESS_KEY="<user_secret_access_key>"
```

**Windows PowerShell**
```
$env:AWS_ACCESS_KEY_ID = "<user_access_key>"
$env:AWS_SECRET_ACCESS_KEY = "<user_secret_access_key>"
```

**Linux**
```
export AWS_ACCESS_KEY_ID="<user_access_key>"
export AWS_SECRET_ACCESS_KEY="<user_secret_access_key>"
```

## Terraform Setup
Create terraform workspaces
```
terraform workspace new Development
terraform workspace new Staging
terraform workspace new Production
```

Select terraform workspace to provision infrastructure
```
terraform select Development
```

Get the current workspace
```
terraform show Development
```
## KUBECTL
Update config file
```
aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)
```
## ARGOCD
Get ArgoCD credentials.
```
kubectl get secret argocd-initial-admin-secret -n argocd -o=jsonpath='{.data.password}'
```