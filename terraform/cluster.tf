#----------------------------------------------------------------------------------------
# @name:    cluster.tf
# @author:  Andrés ROCHA
#----------------------------------------------------------------------------------------

data "aws_caller_identity" "current" {}
data "aws_eks_cluster" "selected" {
  name = "${terraform.workspace}-${var.eks-cluster-name}"
  depends_on = [
    aws_eks_cluster.eks-cluster
  ]
}
resource "aws_iam_openid_connect_provider" "default" {
  url = data.aws_eks_cluster.selected.identity[0].oidc[0].issuer
  client_id_list = [ "sts.amazonaws.com" ]
  thumbprint_list = []
}

# Policies ------------------------------------------------------------------------------
resource "aws_iam_policy" "AWSLoadBalancerControllerIAMPolicy" {
  name    = "AWSLoadBalancerControllerIAMPolicy"
  policy  = file( "./policies/alb-controller-policy.json" )
}
# Policy Documents ----------------------------------------------------------------------
data "aws_iam_policy_document" "AmazonEKSLoadBalancerControllerRole" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]
    principals {
      identifiers = [ 
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${replace(data.aws_eks_cluster.selected.identity[0].oidc[0].issuer, "https://", "")}"
      ]
      type = "Federated"
    }
    condition {
      test     = "StringEquals"
      variable = "${replace(data.aws_eks_cluster.selected.identity[0].oidc[0].issuer, "https://", "")}:sub"
      values = [ "system:serviceaccount:kube-system:aws-load-balancer-controller" ]
    }
    condition {
      test      = "StringEquals"
      variable  = "${replace(data.aws_eks_cluster.selected.identity[0].oidc[0].issuer, "https://", "")}:aud"
      values    = [ "sts.amazonaws.com" ]
    }
  }
}

# Roles ---------------------------------------------------------------------------------
resource "aws_iam_role" "eks-cluster-role" {
  name               = "${terraform.workspace}-EKS-Cluster-Role"
  assume_role_policy = file( "./policies/eks-cluster-role-policy.json" )

  tags = {
    Environment = terraform.workspace
  }
}
resource "aws_iam_role" "eks-node-role" {
  name               = "${terraform.workspace}-EKS-Node-Role"
  assume_role_policy = file( "./policies/eks-node-role-policy.json" )

  tags = {
    Environment = terraform.workspace
  }
}
resource "aws_iam_role" "eks-alb-controller-role" {
  name               = "${terraform.workspace}-EKS-ALB-Role"
  assume_role_policy = data.aws_iam_policy_document.AmazonEKSLoadBalancerControllerRole.json

  tags = {
    Environment = terraform.workspace
  }
}

# Policy Attachments --------------------------------------------------------------------
resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-cluster-role.name
}
resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks-node-role.name
}
resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks-node-role.name
}
resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks-node-role.name
}
resource "aws_iam_role_policy_attachment" "AmazonEKSLoadBalancerControllerRole" {
  policy_arn = aws_iam_policy.AWSLoadBalancerControllerIAMPolicy.arn
  role       = aws_iam_role.eks-alb-controller-role.name
}

# EKS Cluster ---------------------------------------------------------------------------
resource "aws_eks_cluster" "eks-cluster" {
  name     = "${terraform.workspace}-${var.eks-cluster-name}"
  version  = var.kubernetes-version
  role_arn = aws_iam_role.eks-cluster-role.arn

  vpc_config {
    subnet_ids = aws_subnet.eks-private-subnet[*].id
  }
  tags = {
    Environment = terraform.workspace
  }
}

# EKS Node Groups -----------------------------------------------------------------------
resource "aws_eks_node_group" "eks-node-group" {
  cluster_name    = aws_eks_cluster.eks-cluster.name
  node_group_name = "${terraform.workspace}-${var.eks-cluster-name}-Node-Group"
  node_role_arn   = aws_iam_role.eks-node-role.arn
  subnet_ids      = aws_subnet.eks-private-subnet[*].id
  instance_types  = var.eks-nodes-instance-type

  scaling_config {
    desired_size = 2
    max_size     = 2
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly
  ]
}

# Kubernetes Service Accounts -----------------------------------------------------------
resource "kubernetes_service_account" "this" {
  #automount_service_account_token = true
  metadata {
    name      = "aws-load-balancer-controller"
    namespace = "kube-system"

    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.eks-alb-controller-role.arn
    }
    labels = {
      "app.kubernetes.io/name"        = "aws-load-balancer-controller"
      "app.kubernetes.io/component"   = "controller"
    }
  }
}

# Outputs -------------------------------------------------------------------------------
output "cluster-endpoint" {
  value = aws_eks_cluster.eks-cluster.endpoint
}
output "cluster-name" {
  value = aws_eks_cluster.eks-cluster.name
}