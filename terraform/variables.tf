#----------------------------------------------------------------------------------------
# @name:    variables.tf
# @author:  Andrés ROCHA
#----------------------------------------------------------------------------------------

variable "aws-region" {
  type    = string
  default = "us-east-1"
}

variable "vpc-cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "availability-zones-count" {
  type    = number
  default = 2
}

variable "eks-cluster-name" {
  type    = string
  default = "eks-cluster"
}

variable "kubernetes-version" {
  type    = number
  default = 1.21
}
variable "eks-nodes-instance-type" {
  type    = list
  default = ["m5.large"]
}