#----------------------------------------------------------------------------------------
# @name:    network.tf
# @author:  Andrés ROCHA
#----------------------------------------------------------------------------------------

# Data-----------------------------------------------------------------------------------
data "aws_availability_zones" "available" {}

# VPC -----------------------------------------------------------------------------------
resource "aws_vpc" "eks-vpc" {
  cidr_block           = var.vpc-cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name        = "${terraform.workspace} EKS VPC"
    Environment = terraform.workspace
  }
}

# Internet Gateway ----------------------------------------------------------------------
resource "aws_internet_gateway" "eks-internet-gateway" {
  vpc_id = aws_vpc.eks-vpc.id

  tags = {
    Name        = "${terraform.workspace} EKS IGW"
    Environment = terraform.workspace
  }
  depends_on = [
    aws_vpc.eks-vpc
  ]
}

# Public Subnets ------------------------------------------------------------------------
resource "aws_subnet" "eks-public-subnet" {
  count = var.availability-zones-count

  vpc_id                  = aws_vpc.eks-vpc.id
  cidr_block              = cidrsubnet(var.vpc-cidr, 8, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  tags = {
    Name                     = "${terraform.workspace} EKS Public Subnet ${count.index}"
    Environment              = terraform.workspace
    "kubernetes.io/role/elb" = 1
  }
  depends_on = [
    aws_vpc.eks-vpc
  ]
}

# Private Subnets -----------------------------------------------------------------------
resource "aws_subnet" "eks-private-subnet" {
  count = var.availability-zones-count

  vpc_id            = aws_vpc.eks-vpc.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = cidrsubnet(var.vpc-cidr, 8, count.index + var.availability-zones-count)

  tags = {
    Name                              = "${terraform.workspace} EKS Private Subnet ${count.index}"
    Environment                       = terraform.workspace
    "kubernetes.io/role/internal-elb" = 1
  }
  depends_on = [
    aws_vpc.eks-vpc
  ]
}

# NAT Elastic IP ------------------------------------------------------------------------
resource "aws_eip" "eks-elastic-ip" {
  count = var.availability-zones-count

  vpc = true

  tags = {
    Name        = "${terraform.workspace} Elastic IP ${count.index}"
    Environment = terraform.workspace
  }
}

# NAT Gateway ---------------------------------------------------------------------------
resource "aws_nat_gateway" "eks-nat-gateway" {
  count = var.availability-zones-count

  allocation_id = aws_eip.eks-elastic-ip[count.index].id
  subnet_id     = aws_subnet.eks-public-subnet[count.index].id

  tags = {
    Name        = "${terraform.workspace} NAT Gateway ${count.index}"
    Environment = terraform.workspace
  }
}

# Route Tables --------------------------------------------------------------------------
resource "aws_route_table" "eks-public-route-table" {
  vpc_id = aws_vpc.eks-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.eks-internet-gateway.id
  }
  tags = {
    Name        = "${terraform.workspace} EKS Public Subnets Route Table"
    Environment = terraform.workspace
  }
  depends_on = [
    aws_vpc.eks-vpc,
    aws_internet_gateway.eks-internet-gateway
  ]
}

resource "aws_route_table" "eks-private-route-table" {
  count = var.availability-zones-count

  vpc_id = aws_vpc.eks-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.eks-nat-gateway[count.index].id
  }
  tags = {
    Name        = "${terraform.workspace} Private Subnets Route Table ${count.index}"
    Environment = terraform.workspace
  }
  depends_on = [
    aws_vpc.eks-vpc
  ]
}

# Route Table Associations --------------------------------------------------------------
resource "aws_route_table_association" "eks-public-route-table-assoc" {
  count = var.availability-zones-count

  subnet_id      = aws_subnet.eks-public-subnet[count.index].id
  route_table_id = aws_route_table.eks-public-route-table.id

  depends_on = [
    aws_route_table.eks-public-route-table
  ]
}

resource "aws_route_table_association" "private-route-table-assoc" {
  count = var.availability-zones-count

  subnet_id      = aws_subnet.eks-private-subnet[count.index].id
  route_table_id = aws_route_table.eks-private-route-table[count.index].id
}