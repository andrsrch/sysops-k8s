#----------------------------------------------------------------------------------------
# @name:    security-groups.tf
# @author:  Andrés ROCHA
#----------------------------------------------------------------------------------------

# Control Plane -------------------------------------------------------------------------
resource "aws_security_group" "eks-control-plane-sg" {
  name        = "${terraform.workspace} Control Plane SG"
  vpc_id      = aws_vpc.eks-vpc.id
  description = "Control Plane Security Group"

  tags = {
    Name        = "${terraform.workspace} Control Plane SG"
    Environment = terraform.workspace
  }
  depends_on = [aws_vpc.eks-vpc]
}

resource "aws_security_group_rule" "eks-control-plane-outbound-rule" {
  security_group_id = aws_security_group.eks-control-plane-sg.id
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}
