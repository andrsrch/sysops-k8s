#----------------------------------------------------------------------------------------
# @name:    version.tf
# @author:  Andrés ROCHA
#----------------------------------------------------------------------------------------

terraform {
  required_providers {
    aws = {
      source  = "registry.terraform.io/hashicorp/aws"
      version = "~> 3.27"
    }
    kubernetes = {
      source  = "registry.terraform.io/hashicorp/kubernetes"
      version = "~> 2.12.1"
    }
    kubectl = {
      source  = "registry.terraform.io/gavinbunney/kubectl"
      version = "1.14.0"
    }
  }
  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = var.aws-region
}

provider "kubernetes" {
  host                    = aws_eks_cluster.eks-cluster.endpoint
  cluster_ca_certificate  = base64decode(aws_eks_cluster.eks-cluster.certificate_authority[0].data)
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args = [ "eks", "get-token", "--cluster-name", aws_eks_cluster.eks-cluster.name, "--region", var.aws-region ]
    command = "aws"
 }
}

provider "kubectl" {
  host                    = aws_eks_cluster.eks-cluster.endpoint
  cluster_ca_certificate  = base64decode(aws_eks_cluster.eks-cluster.certificate_authority[0].data)
  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    args = [ "eks", "get-token", "--cluster-name", aws_eks_cluster.eks-cluster.name, "--region", var.aws-region ]
    command = "aws"
 }
}