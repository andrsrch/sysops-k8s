#----------------------------------------------------------------------------------------
# @name:    kubernetes.tf
# @author:  Andrés ROCHA
#----------------------------------------------------------------------------------------

# Data ----------------------------------------------------------------------------------
data "kubectl_file_documents" "cert-manager" {
  content = file("./manifests/cert-manager.yml")
}
data "kubectl_file_documents" "alb" {
  content = templatefile("./manifests/alb.yml", { cstm-cluster-name = "${terraform.workspace}-${var.eks-cluster-name}"})
}
data "kubectl_file_documents" "argocd" {
  content = file("./manifests/argocd.yml")
}
data "kubectl_file_documents" "statics" {
  content = file("./manifests/nginx.yml")
}
data "kubectl_file_documents" "echo" {
  content = file("./manifests/echoserver.yml")
}

# Deployments ---------------------------------------------------------------------------
resource "kubectl_manifest" "cert-manager-deployment" {
  for_each            = data.kubectl_file_documents.cert-manager.manifests
  yaml_body           = each.value
  validate_schema     = false

  depends_on = [
    data.kubectl_file_documents.cert-manager
  ]
}
resource "kubectl_manifest" "alb-deployment" {
  for_each            = data.kubectl_file_documents.alb.manifests
  yaml_body           = each.value
  validate_schema     = false
  override_namespace  = "kube-system"

  depends_on = [
    kubectl_manifest.cert-manager-deployment
  ]
}
resource "kubectl_manifest" "argocd-deployment" {
  for_each            = data.kubectl_file_documents.argocd.manifests
  yaml_body           = each.value
  validate_schema     = false
  override_namespace  = "argocd"

  depends_on = [
    kubectl_manifest.alb-deployment
  ]
}
resource "kubectl_manifest" "statics-deployment" {
  for_each            = data.kubectl_file_documents.statics.manifests
  yaml_body           = each.value
  validate_schema     = false

  depends_on = [
    kubectl_manifest.argocd-deployment
  ]
}
resource "kubectl_manifest" "echo-deployment" {
  for_each            = data.kubectl_file_documents.echo.manifests
  yaml_body           = each.value
  depends_on = [
    kubectl_manifest.argocd-deployment
  ]
}